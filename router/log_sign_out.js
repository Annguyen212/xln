module.exports = function(app, passport){
    app.get("/signup", function(req,res){
            res.render('page/signup',{message: req.flash('megsign')});
        })
    app.post("/signup", passport.authenticate('signup',{
            successRedirect: '/login',
            failureRedirect: '/signup',
            failureFlash: true
        }));
    app.get("/login", function(req,res){
            res.render('page/login',{message: req.flash('meglog')});
        })
    app.post("/login", passport.authenticate('login',{
            successRedirect: '/',
            failureRedirect: '/login',
            failureFlash:true
        },function(req,res){
            if (req.body.remember)
              req.session.cookie.maxAge = 1000*60*3;
            else 
              req.session.cookie.expires = false;
            res.redirect('/')
        }))
    app.get('/profile',Isloggedin,function(req,res){
        res.render('page/profile',{user: req.user});
    });
    app.get("/logout",function(req,res){
        req.logout();
        res.redirect('/');
    });
}
function Isloggedin(req, res, next){
    if(req.isAuthenticate())
      return next;
    res.redirect('/');
}