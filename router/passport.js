const mysql = require("mysql");
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require("bcrypt-nodejs");

var connect = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "",
    databate: "xln"
});
connect.connect(function(err){
    if(err)
      console.log("error while connect mysql");
    else
      console.log("mysql is connected")
});

module.exports = function(passport){
    passport.serializeUser(function(user, done){
        done(null, user.id);
    });

    passport.deserializeUser(function(id,done){
        connect.query('SELECT * FROM users WHERE id = ?',[id],
        function(err,rows){
            done(err,rows[0]);
        });
    });

    passport.use('signup',
    new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password',
        passReqToCallback: true
    },function(req, username, password, done){
        connect.query('SELECT * FROM users WHERE username = ?',[username],
        function(err,rows){
            if(err)
              return done(err);
            if(rows.length)
              return done(null, false, req.flash('megsign','Tai khoan da ton tai!!'));
            else{
                var newuser ={
                    username: username,
                    password: bcrypt.hashSync(password, null, null),
                    firstname: req.body.firstname,
                    lastname:req.body.lastname
                };
                connect.query('INSERT INTO users (username, password, firstname, lastname) VALUES (?,?)',[newuser.username,newuser.password,newuser.firstname,newuser.lastname],
                function(err,rows){
                    newuser.id= rows.insertId;
                    return done(null, newuser);
                });
            }
        });
    })
    );

    passport.use('login',
    new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password',
        passReqToCallback: true
    },function(req, username, password, done){
        connect.query('SELECT * FROM users WHERE username = ?',[username],
        function(err, rows){
            if(err) 
              return done(err);
            if(!rows.length)
              return done(null, false, req.flash('meglog','Ten truy cap va mat khau khong chinh xac'));
            if(!bcrypt.compareSync(password, rows[0].password))
              return done(null, false, req.flash('meglog','ten truy cap va mat khau khong chinh xac'));
            return done(null, rows[0]);
        });
    })
    );
};