const express = require("express");
var router = express.Router();
// data insert ejs
const most = require('./data_json/most');
const trend = require('./data_json/trend');
const amnhac = require('./data_json/amnhac');
const dienanh = require('./data_json/dienanh');
const doisong = require('./data_json/doisong');
const dovui = require('./data_json/dovui');
const fashion = require('./data_json/fashion');
const gioitre = require('./data_json/gioitre');
const hocduong = require('./data_json/hocduong');
const khoahoc = require('./data_json/khoahoc');
const news = require('./data_json/news');
const thethao = require('./data_json/thethao');


// home page
router.get('/', function (req, res) {
    console.log("home page open");
    console.log(req.user);
    res.render('page/home', {
        most: most,
        trend: trend,
        amnhac: amnhac,
        dienanh: dienanh,
        doisong: doisong,
        dovui: dovui,
        fashion: fashion,
        gioitre: gioitre,
        hocduong: hocduong,
        khoahoc: khoahoc,
        news: news,
        thethao: thethao
    });
})
// doi song page
router.get('/', function (req, res) {
    res.render('page/doi_song', {
        most: most,
        trend: trend,
        doisong: doisong
    });
})
// fashion page
router.get('/', function (req, res) {
    res.render('page/fashion', {
        most: most,
        trend: trend,
        fashion: fashion
    });
})
// gioi tre page
router.get('/', function (req, res) {
    res.render('page/gioi_tre', {
        most: most,
        trend: trend,
        gioitre: gioitre
    });
})
// am nhac page
router.get('/', function (req, res) {
    res.render('page/am_nhac', {
        most: most,
        trend: trend,
        amnhac: amnhac
    });
})
// dien anh page
router.get('/', function (req, res) {
    res.render('page/dien_anh', {
        most: most,
        trend: trend,
        dienanh: dienanh
    });
})
// khoa hoc page
router.get('/', function (req, res) {
    res.render('page/khoa_hoc', {
        most: most,
        trend: trend,
        khoahoc: khoahoc
    });
})
// the thao page
router.get('/', function (req, res) {
    res.render('page/the_thao', {
        most: most,
        trend: trend,
        thethao: thethao
    });
})
// hoc duong page
router.get('/', function (req, res) {
    res.render('page/hoc_duong', {
        most: most,
        trend: trend,
        hocduong: hocduong
    });
})
// do vui page
router.get('/', function (req, res) {
    res.render('page/do_vui', {
        most: most,
        trend: trend,
        dovui: dovui
    });
})


module.exports = router;