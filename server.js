const express = require("express");
const router = require("./router/router.js");
const session = require("express-session");
const cookieParser = require("cookie-parser");
const bodyparser = require("body-parser");
const morgan = require("morgan");
const passport = require("passport");
const flash = require("connect-flash");
const app = express();
var port = process.env.port || 8080;
require('./router/passport')(passport);

// set project file
app.use(express.static(__dirname));
//
app.use(morgan('dev'));
app.use(cookieParser());
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended: false}));

// set view engine and file public(static)
app.use('/public', express.static(__dirname + '/public'));
app.set('view engine', 'ejs');

// config session
app.use(session({
    secret: 'justasecret',
    resave: true,
    saveUninitialized: true
}));
// set pastport
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());


//router
require("./router/log_sign_out")(app,passport);
app.use(router);

//listen
app.listen(port, function () {
    console.log("server listenning on port:" + port);
});

