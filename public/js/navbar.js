window.onload = function () {
    window.jQuery ? $(document).ready(function () {
        $(".sidebarNavigation-left .navbar-collapse").hide().clone().appendTo("body").removeAttr("class").addClass("sideMenu-left").show(),
            $("body").append("<div class='overlay-left'></div>"),
            $(".sideMenu-left").addClass($(".sidebarNavigation-left").attr("data-sidebarClass")),
            $(".navbar-toggle, .navbar-toggler").on("click", function () {
                $(".sideMenu-left, .overlay-left").toggleClass("open"),
                    $(".overlay-left").on("click", function () {
                        $(this).removeClass("open"),
                            $(".sideMenu-left").removeClass("open")
                    })
            }),
            $("body").on("click", ".sideMenu-left.open .nav-item", function () {
                $(this).hasClass("dropdown") || $(".sideMenu-left, .overlay-left").toggleClass("open")
            }),
            $(window).resize(function () { 
                $(".navbar-toggler").is(":hidden") ? $(".sideMenu-left, .overlay-left").hide() : $(".sideMenu-left, .overlay-left").show() })
    }) : console.log("sidebarNavigation-left Requires jQuery")


    window.jQuery ? $(document).ready(function () {
        $(".sidebarNavigation-right .navbar-collapse").hide().clone().appendTo("body").removeAttr("class").addClass("sideMenu-right").show(),
            $("body").append("<div class='overlay-right'></div>"),
            $(".sideMenu-right").addClass($(".sidebarNavigation-right").attr("data-sidebarClass")),
            $(".navbar-toggle, .navbar-toggler").on("click", function () {
                $(".sideMenu-right, .overlay-right").toggleClass("open"),
                    $(".overlay-right").on("click", function () {
                        $(this).removeClass("open"),
                            $(".sideMenu-right").removeClass("open")
                    })
            }),
            $("body").on("click", ".sideMenu-right.open .nav-item", function () {
                $(this).hasClass("dropdown") || $(".sideMenu-right, .overlay-right").toggleClass("open")
            }),
            $(window).resize(function () { 
                $(".navbar-toggler").is(":hidden") ? $(".sideMenu-right, .overlay-right").hide() : $(".sideMenu-right, .overlay-right").show() })
    }) : console.log("sidebarNavigation Requires jQuery")
};

